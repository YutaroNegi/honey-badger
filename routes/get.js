var router = require('express').Router();
module.exports = router

router.get('/',async (req, res) => {
    try {
        let id_contato = 0
        let nome = ''
        let perguntas = await req.app.get('sql').get_perguntas_dinamicas(id_contato, 0)
        let pesquisa_info = [{id_pesquisa_dinamica: 0}]
        res.render('home/index', {id_contato, nome, perguntas, pesquisa_info})
    } catch (error) {
        console.log(error);
    }
})

router.get('/id=:id_contato?',async (req, res) => {
    try {
        let id_contato = req.params.id_contato
        let response = await req.app.get('sql').check_id_contato(id_contato)
        let nome = response[0].nome
        let status = response[0].status
        let perguntas = await req.app.get('sql').get_perguntas_dinamicas(id_contato, 0)
        let pesquisa_info = [{id_pesquisa_dinamica: 0}]
        if (status != 1){
            id_contato = 0
            nome = ''
        } 
    
        res.render('home/index', {id_contato, nome, perguntas, pesquisa_info})
    } catch (error) {
        console.log(error);
    }
})

router.get('/pesquisa=:id_pesquisa?-id=:id_contato?',async (req, res) => {
    try {
        let id_contato = req.params.id_contato
        let id_pesquisa = req.params.id_pesquisa
        let response = await req.app.get('sql').check_id_contato(id_contato)
        let nome = response[0].nome
        let status = response[0].status
        let perguntas = await req.app.get('sql').get_perguntas_dinamicas(id_contato, id_pesquisa) 
        if (status != 1){
            id_contato = 0
            nome = ''
        }
        let pesquisa_info = await req.app.get('sql').get_pesquisa_dinamica(id_pesquisa) 

        res.render('home/index', {id_contato, nome, perguntas, pesquisa_info})
    } catch (error) {
        console.log(error);
    }
})