var router = require('express').Router();
module.exports = router

router.post('/cadastrar_usuario_badger', async function(req, res){
    try {
        let resposta = await req.app.get('sql').cadastrar_usuario_badger(req.body)
        
        if(resposta[0].cadastrado == 1){
            let perguntas = await req.app.get('sql').get_perguntas_dinamicas(resposta[0].id_contato, req.body.id_pesquisa)
            resposta[0].perguntas = perguntas
        }

        for(item of req.body.respostas_arr){
            item.id_contato = resposta[0].id_contato
            await req.app.get('sql').insere_pergunta_resposta_dinamica(item)
        }

        if(req.body.pj){
            req.body.id_contato = resposta[0].id_contato
            await req.app.get('sql').cadastrar_empresa_badger(req.body)

            for(item of req.body.respostas_pj_arr){
                item.id_contato = resposta[0].id_contato
                await req.app.get('sql').insere_pergunta_resposta_dinamica(item)
            }
        }
        res.send(resposta)
    } catch (error) {
        res.send(error)
    }
})

router.post('/get_perguntas_dinamicas', async function(req, res){
    try {
        let perguntas = await req.app.get('sql').get_perguntas_dinamicas(req.body.id_contato)

        res.send(perguntas)
    } catch (error) {
        res.send(error)
    }
})

router.post('/send_respostas_dinamicas', async function(req, res){
    try {
        let res_radio = req.body.respostas_radio_arr
        let res_texto = req.body.respostas_texto_arr
        let id_contato = req.body.id_contato
        let response

        if(res_radio.length > 0){
            for(resp of res_radio){
                response = await req.app.get('sql').send_resposta_radio(resp.id_pergunta_dinamica, resp.id_resposta, id_contato)
            }
        }

        if(res_texto.length > 0){
            for(resp of res_texto){
                response = await req.app.get('sql').send_resposta_texto(resp.id_pergunta_dinamica, resp.resposta, id_contato)
            }
        }

        res.send(response)
    } catch (error) {
        res.send(error)
    }
})