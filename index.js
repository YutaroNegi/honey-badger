var express = require('express');
var app = express();
var sql = require('./connection/crm');
var bp = require('body-parser')
const path = require('path')
var serveIndex = require('serve-index')
const fs = require('fs')

const porta = 80

app.set('view engine', 'ejs')
app.use('/', express.static(__dirname + '/views/home'));
app.use(bp.json({limit: '50mb'}))
app.use(express.urlencoded({extended: true}));
app.use('/ftp', express.static('public/ftp'), serveIndex('public/ftp', {'icons': true}))

app.set('sql', sql)
app.get('*', require('./routes/get'))
app.post('*', require('./routes/post'))

app.listen(porta, async function (err) {
    if (err) {throw new Error(err)}

    await sql.new()
    console.log('\n\n')
    console.log('      _.--.');
    console.log("    .'   ` '");
    console.log("     ``'.  .'     .-..");
    console.log("        `.  ``````  .-'");
    console.log("       -'`. )--. .'`");
    console.log("       `-`._   \_`-- ");
    console.log('\n\n')
    console.log("the Honey Badger is Online");
});