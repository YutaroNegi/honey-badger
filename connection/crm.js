var Livedb = require('./Livedb').Livedb
var type = require('./Livedb').TYPES

var tp = {}
var connect = {};
module.exports = connect;

connect.new = function () {
  return new Promise(function (_i, _o) {
    Livedb.gera_key("http://138.68.29.250:3005", "projeto", 'l1v3universyt1')
      .then(function (live_key) {
        tp = new Livedb({
          server: "http://138.68.29.250:3005",
          database: "crm",
          projeto: "projeto",
          key: live_key.key
        })
        console.log('--- Banco Fudido Loaded ---')
        _i()
      }).catch(function (err) {
        throw new Error(err)
      })
  })
}

connect.insere_pergunta_resposta_dinamica = function(data) {
    return tp.sql(`
      EXEC p_insere_pergunta_resposta_dinamica @id_contato, @resposta, @id_pergunta
    `)
    .parameter('id_contato', type.Int, data.id_contato)
    .parameter('resposta', type.NVarChar, data.resposta)
    .parameter('id_pergunta', type.Int, data.id_pergunta)

    .execute()
}

connect.cadastrar_usuario_badger = function(data) {
    return tp.sql(`
      EXEC p_cadastrar_usuario_badger @nome, @sobrenome, @email, @cpf, @rua, @numero, @bairro, @cep
    `)

    .parameter('nome', type.NVarChar, data.nome)
    .parameter('sobrenome', type.NVarChar, data.sobrenome)
    .parameter('email', type.NVarChar, data.email)
    .parameter('cpf', type.NVarChar, data.cpf)
    .parameter('rua', type.NVarChar, data.rua)
    .parameter('numero', type.NVarChar, data.numero)
    .parameter('bairro', type.NVarChar, data.bairro)
    .parameter('cep', type.NVarChar, data.cep) 

    .execute()
}

connect.cadastrar_empresa_badger = function(data) {
    return tp.sql(`
      EXEC p_cadastrar_empresa_badger @empresa, @cnpj, @id_contato
    `)

    .parameter('empresa', type.NVarChar, data.empresa)
    .parameter('cnpj', type.NVarChar, data.cnpj)
    .parameter('id_contato', type.Int, data.id_contato)
    .execute()
}

connect.get_perguntas_dinamicas = function(id_contato, id_pesquisa) {
    return tp.sql(`
      EXEC p_get_perguntas_dinamicas @id_contato, @id_pesquisa
    `)
    .parameter('id_contato', type.Int, id_contato)
    .parameter('id_pesquisa', type.Int, id_pesquisa)
    .execute()
}

connect.send_resposta_radio = function(id_pergunta, id_resposta, id_contato) {
    return tp.sql(`
      EXEC p_send_resposta_radio @id_pergunta, @id_resposta, @id_contato
    `)

    .parameter('id_pergunta', type.Int, id_pergunta)
    .parameter('id_resposta', type.Int, id_resposta)
    .parameter('id_contato', type.Int, id_contato)


    .execute()
}

connect.send_resposta_texto = function(id_pergunta, resposta, id_contato) {
    return tp.sql(`
      EXEC p_send_resposta_texto @id_pergunta, @resposta, @id_contato
    `)

    .parameter('id_pergunta', type.Int, id_pergunta)
    .parameter('resposta', type.NVarChar, resposta)
    .parameter('id_contato', type.Int, id_contato)


    .execute()
}

connect.check_id_contato = function(id_contato) {
  return tp.sql(`
    EXEC p_check_id_contato @id_contato
  `)
  .parameter('id_contato', type.Int, id_contato)
  .execute()
}

connect.get_pesquisa_dinamica = function(id_pesquisa) {
  return tp.sql(`
    EXEC get_pesquisa_dinamica @id_pesquisa
  `)
  .parameter('id_pesquisa', type.Int, id_pesquisa)
  .execute()
}



