$( document ).ready(function() {
    if (id_contato_global != 0) {
        $('#nome_contato_boas_vindas').text(`Bem vindo de volta ${nome_global}!`)
        $('#nome_contato_boas_vindas').removeClass('hide')

        $('#form_cadastro_usuario').addClass('hide')
        $('#form_perguntas').removeClass('hide')

        $('#enviar_resposta_btn').attr('onclick', `send_respostas(${id_contato_global})`)
    }

    if(pesquisa_info) load_pesquisa_info(pesquisa_info)
    get_perguntas(perguntas_global)
});

function cadastrar_usuario() {
    let nome = $('#nome_cadastro_input').val()
    let sobrenome = $('#sobrenome_cadastro_input').val()
    let email = $('#email_cadastro_input').val()
    let cpf = $('#cpf_cadastro_input').val()
    let rua = $('#rua_cadastro_input').val()
    let numero = $('#numero_cadastro_input').val()
    let bairro = $('#bairro_cadastro_input').val()
    let cep = $('#cep_cadastro_input').val()

    let pj = $('#pj_btn').is(':checked')
    let empresa = $('#nome_empresa_cadastro_input').val()
    let cnpj = $('#cnpj_cadastro_input').val()

    nome_cadastrado_global = nome

    let error = false

    if(!nome || !sobrenome || !email || !cpf || !rua || !numero || !bairro || !cep){
        $('.cadastro_input').each(function(){
            if(!$(this).val()){
                $(this).css('border', '1px solid red')
                error = true
            }else{
                $(this).css('border', '1px solid rgba(34,36,38,.15)')
            }
        })
    }

    if((!empresa || !cnpj) && pj){
        $('.empresa_cadastro_input').each(function(){
            if(!$(this).val()){
                $(this).css('border', '1px solid red')
                error = true
            }else{
                $(this).css('border', '1px solid rgba(34,36,38,.15)')
            }
        })
    }

    if(error) return

    $('#loading_cadastro_usuario').addClass('active')

    let respostas_arr = [
        {resposta: nome, id_pergunta: 12},
        {resposta: sobrenome, id_pergunta: 13},
        {resposta: email, id_pergunta: 14},
        {resposta: cpf, id_pergunta: 15},
        {resposta: rua, id_pergunta: 16},
        {resposta: numero, id_pergunta: 17},
        {resposta: bairro, id_pergunta: 18},
        {resposta: cep, id_pergunta: 19},
    ]

    let respostas_pj_arr = [
        {resposta: empresa, id_pergunta: 20},
        {resposta: cnpj, id_pergunta: 21},
    ]
    let data = {nome, sobrenome, email, cpf, rua, numero, bairro, cep, empresa, cnpj, pj, respostas_arr, respostas_pj_arr, id_pesquisa: pesquisa_info[0].id_pesquisa_dinamica}
    console.log(data);

    $.ajax({
        url: '/cadastrar_usuario_badger',
        method: 'post',
        data: data
    }).then((res) => {
        console.log(res);
        $('#enviar_resposta_btn').attr('onclick', `send_respostas(${res[0].id_contato})`)
        $('#form_cadastro_usuario').addClass('hide')
        $('#form_perguntas').removeClass('hide')

        if(res[0].perguntas){
            $('#nome_contato_boas_vindas').text(`Percebemos que já tem cadastro ${nome_cadastrado_global}! Bem vindo de volta! `)
            $('#nome_contato_boas_vindas').removeClass('hide')
    
            $('#form_cadastro_usuario').addClass('hide')
            $('#form_perguntas').removeClass('hide')

            get_perguntas(res[0].perguntas)
        }
    }).fail((err)=>{
        console.log(err);
        $('#loading_cadastro_usuario').removeClass('active')
    })
}

function get_perguntas(perguntas) {
    if(perguntas.length == 0){
        $('#form_perguntas').addClass('hide')
        $('#premio_div').removeClass('hide')
        $('#responda_perguntas_h4').addClass('hide')
    }
    
    let html_pergutas = ''
    perguntas.forEach(pergunta=>{
        switch (pergunta.id_tipo_pergunta) {
            case 1:
                html_pergutas += `
                    <span>${pergunta.pergunta}</span>
                    <div style="margin-bottom: 5%" class="ui form">
                        <div class="grouped fields">
                            <div class="field">
                                <div class="ui radio checkbox">
                                <input class="resposta_radio_input" type="radio" value="1" id_pergunta_dinamica="${pergunta.id_pergunta_dinamica}" name="${pergunta.id_pergunta_dinamica}">
                                <label>Sim</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui radio checkbox">
                                <input class="resposta_radio_input" type="radio" value="2" id_pergunta_dinamica="${pergunta.id_pergunta_dinamica}" name="${pergunta.id_pergunta_dinamica}">
                                <label>Não</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui radio checkbox">
                                <input class="resposta_radio_input" type="radio" value="3" id_pergunta_dinamica="${pergunta.id_pergunta_dinamica}" name="${pergunta.id_pergunta_dinamica}">
                                <label>Não desejo informar</label>
                                </div>
                            </div>
                        </div>
                    </div>
                `
            break;
        
            case 2:
                if(pergunta.id_categoria_pergunta == 1 && id_contato_global == 0) return

                html_pergutas += `
                    <span>${pergunta.pergunta}</span>
                    <div style="margin-bottom: 5%;" class="ui input">
                        <input id_pergunta_dinamica="${pergunta.id_pergunta_dinamica}" class="resposta_text_input" type="text" placeholder="Nome">
                    </div>
                `
            break;
        }
    })
    $('#perguntas_box').empty()
    $('#perguntas_box').append(html_pergutas)
    $('#loading_geral').removeClass('active')
}

function send_respostas(id_contato){
    let error = false
    let radio_error = false

    let respostas_radio_arr = []
    $('.resposta_radio_input').each(function(){
        if($(this).is(':checked')){
            let obj ={
                id_pergunta_dinamica : $(this).attr('id_pergunta_dinamica'),
                id_resposta: $(this).attr('value')
            }

            respostas_radio_arr.push(obj)
        }
    })

    let respostas_texto_arr = []
    $('.resposta_text_input').each(function(){
        if(!$(this).val()){
            $(this).css('border', '1px solid red')
            error = true
        }else{
            $(this).css('border', '1px solid rgba(34,36,38,.15)')
            error = false
        }

        let obj = {
            id_pergunta_dinamica: $(this).attr('id_pergunta_dinamica'),
            resposta: $(this).val()
        }
        respostas_texto_arr.push(obj)
    })

    $('.grouped.fields').each(function(){
        let input = $(this).find('input')
        let temp_error = true

        $(input).each(function(){
            if($(this).is(':checked')){
                temp_error = false
            }
        })

        if(temp_error){
            $(input).each(function(){
                $(this).siblings('label').css('color', 'red')
            })
        }else{
            $(input).each(function(){
                $(this).siblings('label').css('color', 'black')
            })
        }

        if(!radio_error && temp_error){
            radio_error = temp_error
        }
    })

    if(error || radio_error) return

    $('#loading_send_respostas').addClass('active')
    let data = {respostas_radio_arr, respostas_texto_arr, id_contato}

    $.ajax({
        url: '/send_respostas_dinamicas',
        method: 'post',
        data: data
    }).then((res) => {
        console.log(res);
        $('#loading_send_respostas').removeClass('active')
        $('#form_perguntas').addClass('hide')
        $('#premio_div').removeClass('hide')
        $('#responda_perguntas_h4').addClass('hide')
    }).fail((err)=>{
        console.log(err);
        $('#responda_perguntas_h4').addClass('hide')
        $('#loading_send_respostas').removeClass('active')
    })
}

function show_pj_div() {
    if($('#pj_div').hasClass('hide')){
        $('#pj_div').removeClass('hide')
    }else{
        $('#pj_div').addClass('hide')
    }
}

function load_pesquisa_info(pesquisa) {
    $('iframe').remove()
    $('#premio_div').append(pesquisa[0].link_premio)
    $('#enviar_resposta_btn').text(pesquisa[0].texto_btn)
    $('#logo_pesquisa').attr('src', pesquisa[0].logo)
    $('#cabecalho').text( pesquisa[0].cabecalho)
    $('#responda_perguntas_h4').text( pesquisa[0].intro)

    console.log(pesquisa);
}